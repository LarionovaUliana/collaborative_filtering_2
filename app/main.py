import os
import json
import uvicorn

os.environ['CUDA_DEVICE_ORDER'] = "PCI_BUS_ID"
os.environ['CUDA_VISIBLE_DEVICES'] = ""

from fastapi import FastAPI, UploadFile, Query, HTTPException
from model import My_Rec_Model
from info import get_docker_info, logger_init
from utils.const import *
from utils.config import Config

app = FastAPI(
    title=APP_CONSTS['title'],
    description=APP_CONSTS['description'],
    version=APP_CONSTS['version'],
    openapi_tags=APP_CONSTS['tags'],
    swagger_ui_parameters={"defaultModelsExpandDepth": -1}
)

config = Config(
    model_path=MODEL_PATH,
    ratings_test_path=RATINGS_TEST_PATH,
    ratings_train_path=RATINGS_TRAIN_PATH,
    movies_path=MOVIES_PATH,
    users_path=USERS_PATH,
    log_path=LOG_PATH,
    log_eval_path=LOG_EVAL_PATH,
    need_training=NEED_TRAINING,
    logger=logger_init(),
    model_name=MODEL_NAME,
    train_name=TRAIN_NAME,
    test_name=TEST_NAME
)
model = My_Rec_Model(config)
model.warmup()


@app.post("/api/predict", tags=["predict"])
async def predict(
    user_index: int,
    movies_names: list[str],
    movies_ratings: list[float],
    N: int = 5
):
    try:
        top_indices, top_scores = model.predict(user_index, movies_names, movies_ratings, N)
        return json.dumps({"top_indices": top_indices, "ratings": top_scores.tolist()})
    except Exception as ex:
        raise HTTPException(
            detail=str(ex),
            status_code=ex.code if hasattr(object, 'code') else 500
        )

@app.post("/api/similar", tags=["predict"])
async def similar(
    movie_name: str,
    N: int = 5
):
    try:
        movies_ids, name_value = model.find_similar(movie_name, N)
        print(type(movies_ids), type(name_value))
        return json.dumps({"movies_ids": movies_ids, "name": name_value})
    except Exception as ex:
        raise HTTPException(
            detail=str(ex),
            status_code=ex.code if hasattr(object, 'code') else 500
        )

@app.post("/api/reload", tags=["reload"])
async def reload():
    try:
        model = My_Rec_Model(config)
        model.warmup()
        return "model loaded"
    except Exception as ex:
        raise HTTPException(
            detail=str(ex),
            status_code=ex.code if hasattr(object, 'code') else 500
        )
@app.get("/api/info", tags=["info"])
def get_service_info():
    try:
        s = get_docker_info()
        return {
            "credentials": "credentials",
            "docker_build_time": s,
            "model_training_info": "model_training_info"
        }
    except Exception as ex:
        raise HTTPException(
            detail=str(ex),
            status_code=ex.code if hasattr(object, 'code') else 500
        )

@app.get("/api/log", tags=["info"])
def get_service_logs():
    try:
        answer = []
        with open(config.log_path, 'r') as file:
            lines = file.readlines()
            last_10_lines = lines[-10:]
            for line in last_10_lines:
                answer.append(line)

        return json.dumps({"logs": answer})
    except Exception as ex:
        raise HTTPException(
            detail=str(ex),
            status_code=ex.code if hasattr(object, 'code') else 500
        )

if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)