import subprocess
from utils.const import DOCKER_NAME, LOG_PATH
import logging
from pathlib import Path

def get_docker_info():
    result = subprocess.run(['docker', 'inspect', '--format', '{{.Created}}', DOCKER_NAME], capture_output=True, text=True)
    build_date = result.stdout.strip()
    return build_date

def logger_init():
    # Создание логгера
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    # Установка пути для файла лога
    DIR = Path.cwd()

    # Создание обработчика для файла
    file_handler = logging.FileHandler(DIR / LOG_PATH)
    file_handler.setLevel(logging.DEBUG)

    # Создание обработчика для консоли
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)

    # Настройка форматирования сообщений
    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    file_handler.setFormatter(formatter)
    console_handler.setFormatter(formatter)

    # Добавление обработчиков к логгеру
    logger.addHandler(file_handler)
    logger.addHandler(console_handler)

    return logger