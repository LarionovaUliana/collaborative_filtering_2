import pandas as pd
import fire
import json
import numpy as np
import info
import pickle
from info import logger_init
from utils.const import *
from utils.config import Config


from surprise import Dataset
from surprise import Reader
from surprise import KNNBasic, SVD
from surprise import accuracy
from surprise.model_selection import cross_validate

class DataPreparer:
    """Класс для подготовки данных."""

    def __init__(
        self,
        config,
    ) -> None:
        self.config = config
        self.train = None
        self.test = None
        self.user_id_mapping = None
        self.reverse_user_id_mapping = None
        self.movie_id_mapping = None
        self.reverse_movie_id_mapping = None
        self.movies_info = None
        self.train_df = None
        
    def recalculate_user_ids(self, column):
        column = pd.Series(column)
        if self.user_id_mapping is None:
            self.user_id_mapping = {}
        if self.reverse_user_id_mapping is None:
            self.reverse_user_id_mapping = {}
        
        new_ids = column.unique()
        
        for i, id in enumerate(new_ids):
            if id not in self.user_id_mapping:
                self.user_id_mapping[id] = i
                self.reverse_user_id_mapping[i] = id
        
        column = column.apply(lambda x: self.user_id_mapping[x])
        
        return column
    
    def recalculate_movie_ids(self, column):
        column = pd.Series(column)
        if self.movie_id_mapping is None:
            self.movie_id_mapping = {}
        if self.reverse_movie_id_mapping is None:
            self.reverse_movie_id_mapping = {}
        
        new_ids = column.unique()
        
        for i, id in enumerate(new_ids):
            if int(id) not in self.movie_id_mapping:
                self.movie_id_mapping[int(id)] = int(i)
                self.reverse_movie_id_mapping[int(i)] = int(id)
        
        column = column.apply(lambda x: self.movie_id_mapping[x])
        
        return column
    
    def recalculate_back_user_ids(self, column):
        return np.vectorize(lambda x: self.reverse_user_id_mapping[x])(column)

    def recalculate_back_movie_ids(self, column):
        return np.vectorize(lambda x: self.reverse_movie_id_mapping[x])(column)
    
    def norm_ratings(self, df):
        avg_rating = df.groupby('UserID')['Rating'].mean().reset_index()
        df = df.merge(avg_rating, on='UserID', suffixes=('', '_mean'))
        df['Normalized_Rating'] = (df['Rating'] - df['Rating_mean']) / df['Rating_mean']
        return df
    
    def _norm_ratings_for_str(self, df):
        mean = sum(df) / len(df)
        if mean == 0:
            return df
        df = [(x - mean) / mean for x in df]
        return df
    
    @staticmethod
    def restore_rating(self, df):
        # Вычисляем исходный рейтинг
        df['Restored_Rating'] = (df['Normalized_Rating'] + df['Rating_mean']) * df['Rating_mean']
        
        return df
    
    def prepare_data_for_predict(self, user_index, movie_ids, ratings):
        user_index = self.recalculate_user_ids([user_index])[0]
        movie_ids = self.recalculate_movie_ids(movie_ids)
        ratings = self._norm_ratings_for_str(ratings)
        return user_index, movie_ids, ratings
    
    def _save_model(self):
        np.save(self.config.model_path+"/train.npy", self.train.values)
        with open(self.config.model_path+"/user_id_mapping.pkl", 'wb') as file:
            pickle.dump(self.user_id_mapping, file)
        with open(self.config.model_path+"/reverse_user_id_mapping.pkl", 'wb') as file:
            pickle.dump(self.reverse_user_id_mapping, file)
        with open(self.config.model_path+"/movie_id_mapping.pkl", 'wb') as file:
            pickle.dump(self.movie_id_mapping, file)
        with open(self.config.model_path+"/reverse_movie_id_mapping.pkl", 'wb') as file:
            pickle.dump(self.reverse_movie_id_mapping, file)

        self.config.logger.info("Pipeline completed. Results saved in the {}".format(self.config.model_path))

    def get_model(self):
        self._get_model()

    def _get_model(self):
        self.train = pd.DataFrame(np.load(self.config.model_path+"/train.npy"))
        with open(self.config.model_path+"/user_id_mapping.pkl", 'rb') as file:
            self.user_id_mapping = pickle.load(file)
        with open(self.config.model_path+"/reverse_user_id_mapping.pkl", 'rb') as file:
            self.reverse_user_id_mapping = pickle.load(file)
        with open(self.config.model_path+"/movie_id_mapping.pkl", 'rb') as file:
            self.movie_id_mapping = pickle.load(file)
        with open(self.config.model_path+"/reverse_movie_id_mapping.pkl", 'rb') as file:
            self.reverse_movie_id_mapping = pickle.load(file)
        self.train_df = self._prepare_data()


    def _preprocessing_data(self, df):
        df.drop('Timestamp', axis=1, inplace=True)
        df['UserID'] = self.recalculate_user_ids(df['UserID'])
        df['MovieID'] = self.recalculate_movie_ids(df['MovieID'])
        return df
    
    def _log_data(self, df):
        #self.config.logger.info(df.info())
        self.config.logger.info(df.isna().sum().sort_values())
        self.config.logger.info(df[['Rating']].describe().transpose())
        self.config.logger.info(f'Total unique users in the dataset{df["UserID"].nunique()}')
        self.config.logger.info(f'Total unique Movie in the dataset{df["MovieID"].nunique()}')
    
    def prepare_data_test(self):
        df = pd.read_csv(self.config.ratings_test_path, sep='::', names=['UserID', 'MovieID', 'Rating', 'Timestamp'], encoding='latin-1', engine='python')
        df = self._preprocessing_data(df)
        self._log_data(df)
        self.test = df

    def _prepare_data(self):
        train_df = pd.read_csv(self.config.ratings_train_path, sep='::',
                               names=['UserID', 'MovieID', 'Rating', 'Timestamp'], encoding='latin-1', engine='python')
        train_df = self._preprocessing_data(train_df)
        train_df = self.norm_ratings(train_df)
        return train_df
    def prepare_data_train(self):
        self.movies_info = pd.read_csv(self.config.movies_path, sep='::', names=['MovieID', 'Name', 'Genre'], encoding='latin-1', engine='python')
        if self.config.need_training:
            self.train_df = self._prepare_data()
            self.train = train_df.pivot(index='UserID', columns='MovieID', values='Normalized_Rating').sort_index().fillna(0) 
            self._save_model()
            self._log_data(train_df)
        else:
            self._get_model()
        


class Trainer:
    def __init__(self, config):
        self.config = config
        self.user_similarity = None
        self.train_df = None
       
    def _cosine_similarity(self, vector1, vector2=None):
        if isinstance(vector1, pd.DataFrame):
            vector1 = vector1.values
        if vector2 is None:
            vector2 = vector1
        elif isinstance(vector2, pd.DataFrame):
            vector2 = vector2.values

        dot_product = np.dot(vector1, vector2.T)
        norm_product = np.linalg.norm(vector1, axis=1, keepdims=True) * np.linalg.norm(vector2, axis=1, keepdims=True)
        
        return pd.DataFrame(dot_product / norm_product)

    def _save_model(self):
        np.save(self.config.model_path+"/model.npy", self.user_similarity.values)
        self.config.logger.info("Pipeline completed. Results saved in the {}".format(self.config.model_path))
        
    def _get_model(self):
        self.user_similarity = pd.DataFrame(np.load(self.config.model_path+"/model.npy"))

    def load_model(self):
        self._get_model()
    
    def start_train(self, train):
        if not self.config.need_training:
            self.config.logger.info("Load model...")
            self._get_model()
            self.config.logger.info("Model is loaded")
        else:
            self.config.logger.info("Training model...")
            user_similarity = self._cosine_similarity(train.fillna(0))
            self.user_similarity = pd.DataFrame(user_similarity, columns=train.index, index=train.index)
            self._save_model()
            self.config.logger.info("Training model ended")


class My_Rec_Model:
    def __init__(self, config):
        self.config = config
        self.data = DataPreparer(self.config)
        self.model = Trainer(self.config)

    def _init_data(self):
        self.data.prepare_data_train()
    
    def _train(self):
        self.model.start_train(self.data.train)
        
    def warmup(self):
        self.config.logger.info("Starting Pipeline")
        self._init_data()
        self._train()
        self.config.logger.info(f"Pipeline completed. Results saved in the {self.config.model_path}")

    def _cosine_similarity(self, vector1, vector2=None):
        if isinstance(vector1, pd.DataFrame):
            vector1 = vector1.values
        if vector2 is None:
            vector2 = vector1
        elif isinstance(vector2, pd.DataFrame):
            vector2 = vector2.values

        dot_product = np.dot(vector1, vector2.T)
        norm_product = np.linalg.norm(vector1, axis=1, keepdims=True) * np.linalg.norm(vector2, axis=1, keepdims=True)
        
        return pd.DataFrame(dot_product / norm_product)

    def find_similar_user(self, user_index, N=5):
        user_index = self.data.user_id_mapping[user_index]
        similar_users = self.model.user_similarity.loc[user_index]
        weighted_ratings = pd.Series(self.data.train.fillna(0).values.T.dot(similar_users))
        recommendations = weighted_ratings.sort_values(ascending=False)
        known_ratings = self.data.train.iloc[user_index].dropna().index
        recommendations = recommendations.drop(known_ratings)
        return recommendations.index[:N], recommendations.values[:N]
    
    def _add_ratings(self, df, user_index, movie_ids, ratings):
        new_row = pd.Series(0, index=df.columns, name=user_index)
        for movie_id, rating in zip(movie_ids, ratings):
            new_row[movie_id] = rating
        return new_row
    
    def _normalize_scores(self, scores):
        min_score = min(scores)
        max_score = max(scores)
        normalized_scores = [((x - min_score) / (max_score - min_score)) + 1 for x in scores]
        mean = sum(normalized_scores) / len(normalized_scores)
        normalized_scores = [x * mean + mean for x in normalized_scores]
        return normalized_scores
    
    def _predict_rating_user_to_movie(self):
        dot_product = np.dot(self.data.train, self.data.train.T)
        preds = np.dot(dot_product.T, self.data.train)
        return [self._normalize_scores(pred) for pred in preds]

    def _predict_ratings(self, user_index, movie_ids, ratings):
        user_index, movie_ids, ratings = self.data.prepare_data_for_predict(user_index, movie_ids, ratings)
        new_row = self._add_ratings(self.data.train, user_index, movie_ids, ratings)
        dot_product = np.dot(self.data.train, new_row.T)
        preds = np.dot(dot_product.T, self.data.train)
        normal_preds = self._normalize_scores(preds)
        preds[movie_ids] = -100
        sorted_indices = np.argsort(preds)[::-1]
        return sorted_indices, normal_preds, preds
    
    def predict(self, user_index, movie_names, ratings, N=5):
        movie_ids = []
        for movie_name in movie_names:
            movie_ids.append(int(self.data.movies_info.loc[self.data.movies_info['Name'] == movie_name]['MovieID']))
        sorted_indices, normal_preds, preds = self._predict_ratings(user_index, movie_ids, ratings)
        top_indices = sorted_indices[:N]
        normal_preds = np.array(normal_preds)
        top_scores = normal_preds[top_indices]
        top_indices = self.data.recalculate_back_movie_ids(top_indices)
        names = []
        for id in top_indices:
            names.append(str(self.data.movies_info.loc[self.data.movies_info['MovieID'] == id]['Name'].values))
        return names, top_scores
        
    def _get_name(self, movie_id):
        row = self.data.movies_info.loc[self.data.movies_info['MovieID'] == movie_id]
        name_value = row['Name'].values
        return name_value

    def find_similar(self, movie_name, N=5):
        movie_id = self.data.movies_info.loc[self.data.movies_info['Name'] == movie_name]['MovieID']
        return self._find_similar(movie_id.values[0], N)


    def _find_similar(self, movie_id, N):
        if movie_id is None:
            return "Movie not found"
        movie_index = self.data.recalculate_movie_ids([movie_id])[0]
        if movie_index is None:
            return "Movie not found"
        f = pd.Series(self.data.train[movie_index], index=self.data.train.index, name=movie_index)
        similarity_scores = np.dot(self.data.train.fillna(0).T, self.data.train.fillna(0)[movie_index].values.reshape(-1, 1))
        similar_movies_indices = np.argsort(similarity_scores, axis=0)[-N-1:-1][::-1]
        similar_movies = [self.data.recalculate_back_movie_ids(similar_movies_indices)]
        #similar_movies = [self.data.movie_ids[i] for i in similar_movies_indices]
        name_value = []
        movies_ids = []
        for movie_id in similar_movies[0]:
            row = self.data.movies_info.loc[self.data.movies_info['MovieID'] == movie_id[0]]
            name_value.append(str(row['Name'].values[0]))
            movies_ids.append(int(movie_id[0]))
        return movies_ids, name_value


    def evaluate(self, addit_model=False):
        self.data.prepare_data_test()
        self.config.logger.info("Evaluating start")
        if self.model.user_similarity is None:
            self.model.user_similarity = self.model.load_model()

        ratings_test = self.data.test
        num_users = len(ratings_test)
        pred = self._predict_rating_user_to_movie()
        ratings_test['y_pred'] = ratings_test.apply(lambda row: pred[int(row['UserID'])][int(row['MovieID'])], axis=1)
        ratings_test = ratings_test.dropna(subset=['y_pred'])
        error = np.mean(np.abs(ratings_test['y_pred'] - ratings_test['Rating']))
        self.config.logger.info(f"Error:{error}")

        with open(self.config.log_eval_path, 'a') as f:
            f.write("Model Evaluation\n")
            f.write("Number of Users: {}\n".format(num_users))
            f.write("Average Error: {}\n".format(error / num_users))
            f.write("\n")
        self.config.logger.info("Evaluation completed. Results saved in the log file.")
        if addit_model:
            self._evaluate_KNN()
            self._evaluate_SVD()
        return error

    def _evaluate_KNN(self):
        self.config.logger.info("Старт доп модели KNN")
        reader = Reader(rating_scale=(1, 5))

        train_data = self.data.train_df[['UserID', 'MovieID', 'Rating']]
        test_data = self.data.test[['UserID', 'MovieID', 'Rating']]

        trainset = Dataset.load_from_df(train_data, reader).build_full_trainset()
        testset = Dataset.load_from_df(test_data, reader)

        self.config.logger.info("Обучение модели KNNBasic")
        model = KNNBasic()
        model.fit(trainset)

        self.config.logger.info("Оценка качества предсказаний на тестовом наборе")
        predictions = model.test(testset.build_full_trainset().build_testset())
        error = accuracy.rmse(predictions)
        self.config.logger.info(f"Error for KNN:{error}")

    def _evaluate_SVD(self):
        self.config.logger.info("Старт доп модели SVD")

        train_data = self.data.train_df[['UserID', 'MovieID', 'Rating']]
        test_data = self.data.test[['UserID', 'MovieID', 'Rating']]

        reader = Reader(rating_scale=(1, 5))
        trainset = Dataset.load_from_df(train_data, reader).build_full_trainset()

        self.config.logger.info("Обучение модели SVD")
        model = SVD()
        model.fit(trainset)

        testset = [(row['UserID'], row['MovieID'], row['Rating']) for index, row in test_data.iterrows()]
        predictions = model.test(testset)

        error = accuracy.rmse(predictions)
        self.config.logger.info(f"Error SVD:{error}")

def init_config(model_name=MODEL_NAME, train_name=TRAIN_NAME, test_name=TEST_NAME, need_training=True):
    config = Config(
        model_path=MODEL_PATH,
        ratings_test_path=RATINGS_TEST_PATH,
        ratings_train_path=RATINGS_TRAIN_PATH,
        movies_path=MOVIES_PATH,
        users_path=USERS_PATH,
        log_path=LOG_PATH,
        log_eval_path=LOG_EVAL_PATH,
        need_training=need_training,
        logger=logger_init(),
        model_name=model_name,
        train_name=train_name,
        test_name=test_name
    )
    return config

class CLI():
    def train(self, name='ratings_train.dat', need_training=True):
        config = init_config(train_name=name, need_training=need_training)
        model = My_Rec_Model(config)
        model.warmup()
    def evaluate(self, name='ratings_test.dat', need_training=False, addit_model=False):
        config = init_config(test_name=name, need_training=need_training)
        model = My_Rec_Model(config)
        model.warmup()
        model.evaluate(addit_model)

    def warmup(self, name='ratings_train.dat',  need_training=False):
        try:
            config = init_config(model_name=name, need_training=need_training)
            model = My_Rec_Model(config)
            model.warmup()
        except Exception as ex:
            raise Exception(
                detail=str(ex),
                status_code=ex.code if hasattr(object, 'code') else 500
            )
    def predict(
            self,
            user_index: int,
            movies_names: list[str],
            movies_ratings: list[float],
            N: int = 5
    ):
        try:
            config = init_config(need_training=False)
            model = My_Rec_Model(config)
            model.warmup()
            top_indices, top_scores = model.predict(user_index, movies_ratings[0], movies_ratings[1], N)
            return json.dumps({"top_indices": top_indices.tolist(), "ratings": top_scores.tolist()})
        except Exception as ex:
            raise Exception(
                detail=str(ex),
                status_code=ex.code if hasattr(object, 'code') else 500
            )
    def find_similar(self, movie_name, N=5):
            config = init_config(need_training=False)
            model = My_Rec_Model(config)
            model.warmup()
            movies_ids, name_value = model.find_similar(movie_name, N)
            return json.dumps({"movies_ids": movies_ids, "name": name_value})



if __name__ == '__main__':
  fire.Fire(CLI)