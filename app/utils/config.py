from dataclasses import dataclass

@dataclass
class Config:
    movies_path: str
    ratings_test_path: str
    test_name:str
    ratings_train_path: str
    train_name:str
    users_path: str
    model_path: str
    model_name: str
    log_path: str
    log_eval_path: str
    need_training: bool
    logger: None
