from pathlib import Path

MODEL_PATH = 'data/model'
RATINGS_TEST_PATH = 'data/ratings_test.dat'
RATINGS_TRAIN_PATH = 'data/ratings_train.dat'
MOVIES_PATH = 'data/movies.dat'
USERS_PATH = 'data/users.dat'
LOG_PATH = 'data/log_file.log'
LOG_EVAL_PATH = 'data/log_file.log'
NEED_TRAINING = False
MODEL_NAME = "/model.npy"
TRAIN_NAME = "/ratings_train.dat"
TEST_NAME = "/ratings_test.dat"


DOCKER_NAME='collaborative_filtering'

DIR = Path.cwd()
MUSE_PATH = Path('/models/MUSE')  # путь к модели
INPUT_DATA_PATH = DIR / 'app/service_data/input_data'  # путь к входным данным
OUTPUT_PATH = DIR / 'app/service_data/output_data'  # путь к выходным данным

TOP_K = 10  # сколько ближайших векторов брать при поиске
TOP_K_ALIAS = 'Кол-во похожих текстов'
THRESHOLD = 0.8  # порог для cosine similarity
THRESHOLD_ALIAS = 'Порог схожести'


_openapi_tags = [{
    "name": "Intent mining",
    "description": """""",
}]

APP_CONSTS = {
    'title': "Коллаборативная фильтрация ⛏️",
    'description': """
## Инструкция 📑

    """,
    'tags': _openapi_tags,
    'version': "1.0.0"
}
