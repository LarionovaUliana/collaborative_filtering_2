# collaborative_filtering

## Учетные данные
Ларионова Ульяна
гр. 972102

## Getting started
Создать и настроить вирутальное окружение
```
python -m venv collab_filt 
sourse collab_filt/bin/activate
git remote add origin https://gitlab.com/LarionovaUliana/collaborative_filtering.git
cd collab_filt/collaborative_filtering/
pip install poetry==1.1.6
poetry install
```


## Поднять FastApi приложение
```
 python main.py
```

- ```/api/predict```. Получает список [[movie_name_1, movie_name_2, .., movie_name_N ], [rating_1, rating_2, .., rating_N]] и возвращает TOP M (по умолчанию 20, также параметр) рекомендуемых фильмов с соответствующим оценочным рейтингом. Сортировка по убыванию. [[movie_name_1, movie_name_2, .., movie_name_M], [rating_1, rating_2, .., rating_M]]
- ```/api/log```. Последние 20 строк журнала.
- ```/api/info```. Служебная информация: Ваши учетные данные, дата и время сборки образа Docker, Дата, время и показатели обучения развернутой в данный момент модели.
- ```/api/reload```. Перезагрузите модель .
- ```/api/similar```. возвращает список похожих фильмов {"movie_name": "Lord of the Rings"}

## CLI интерфейс
Команды подаются в формате ```python model.py train```

- ```train```. Получает имя файла набора данных. Выполняет обучение модели. Сохраняет артефакты в ./model/. Регистрирует результаты.
- ```evaluate```. Получает имя файла dataset. Загружает модель из ./data/model/. Оценивает модель с помощью предоставленного набора данных, распечатывает результаты и сохраняет их в журнале.
- ```predict```. Получает список [[movie_id_1, movie_id_2, .., movie_id_N ], [rating_1, rating_2, .., rating_N]] и возвращает TOP M (также параметр) рекомендуемых фильмов с оценочным рейтингом в том же формате, что и входной.
- ```warmup```. Загружает модель из ./data/model/. Обновляет модель, если она уже загружена.
- ```find_similar```. Возвращает N (параметр, по умолчанию = 5) наиболее похожих фильмов для ввода movie_id. Возвращает список [[movie_id_1, movie_id_2, .., movie_id_N ], [[movie_name_1, movie_name_2, .., movie_name_N]]] с сортировкой по убыванию сходства.


## Ссылки
